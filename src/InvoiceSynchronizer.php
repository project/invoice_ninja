<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 * Provides services to synchronize data with Invoice Ninja.
 */
final class InvoiceSynchronizer extends SynchronizerBase {

  protected const INVOICE_NINJA_COLLECTION_INVOICE = 'invoice_ninja.sync_invoice';

  /**
   * The client synchronizer.
   *
   * @var \Drupal\invoice_ninja\ClientSynchronizer
   */
  protected ClientSynchronizer $clientSynchronizer;

  /**
   * The VAT Synchronizer.
   *
   * @var \Drupal\invoice_ninja\VatSynchronizer
   */
  protected VatSynchronizer $vatSynchronizer;

  /**
   * Constructs an InvoiceNinjaApi object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The key/value factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param ClientSynchronizer $clientSynchronizer
   *   The client synchronizer.
   * @param VatSynchronizer $vatSynchronizer
   *   The VAT synchronizer.
   */
  public function __construct(ConfigFactoryInterface $configFactory, KeyValueFactoryInterface $keyValueFactory, EntityTypeManagerInterface $entityTypeManager, ClientSynchronizer $clientSynchronizer, VatSynchronizer $vatSynchronizer) {
    parent::__construct($configFactory, $keyValueFactory, $entityTypeManager);
    $this->clientSynchronizer = $clientSynchronizer;
    $this->vatSynchronizer = $vatSynchronizer;
  }

  /**
   * Return the status if the entity has already been synced.
   *
   * @param \Drupal\invoice_ninja\SyncableInvoiceInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE, if the entity has already been synced, FALSE otherwise.
   */
  public function isSynced(SyncableInvoiceInterface $entity): bool {
    return $this->keyValueFactory->get(self::INVOICE_NINJA_COLLECTION_INVOICE)
      ->get($entity->id()) !== NULL;
  }

  /**
   * Syncs the invoice with Invoice Ninja.
   *
   * @param \Drupal\invoice_ninja\SyncableInvoiceInterface $invoice
   *   The invoice.
   */
  public function syncInvoice(SyncableInvoiceInterface $invoice): void {
    $remoteClientId = $this->clientSynchronizer->syncClient($invoice->getClient());
    $invoiceData = $this->keyValueFactory->get(self::INVOICE_NINJA_COLLECTION_INVOICE)
      ->get($invoice->id());
    if (!$invoiceData) {
      $remoteInvoice = $this->getInvoiceNinjaClient()->invoices->create(
        $this->getInvoiceData($remoteClientId, $invoice)
      );
      $this->storeId(self::INVOICE_NINJA_COLLECTION_INVOICE,
        $invoice->id(), $remoteInvoice['data']['id']);
    }
    else {
      $lastSync = $invoiceData['last_sync'];
      // @phpstan-ignore-next-line
      if ($invoice->getChangedTime() > $lastSync) {
        $remoteInvoiceId = $invoiceData['remote_id'];
        $this->getInvoiceNinjaClient()->clients->update($remoteInvoiceId,
          $this->getInvoiceData($remoteClientId, $invoice)
        );
        $this->storeId(self::INVOICE_NINJA_COLLECTION_INVOICE,
          $invoice->id(), $remoteInvoiceId);
      }
    }
  }

  /**
   * Gets the data for the API call for an invoice for a given client.
   *
   * @param string $clientId
   *   The client ID.
   * @param \Drupal\invoice_ninja\SyncableInvoiceInterface $invoice
   *   The invoice.
   *
   * @return array
   *   The invoice data for the API.
   */
  private function getInvoiceData(string $clientId, SyncableInvoiceInterface $invoice): array {
    $lineItems = [];
    $isVatFree = $invoice->getClient()->isVatFree();
    foreach ($invoice->getLineItems() as $lineItem) {
      $vat = $lineItem->getVat();
      if ($vat !== NULL) {
        $this->vatSynchronizer->syncVat($vat);
      }
      if ($isVatFree) {
        $vat = NULL;
      }
      $lineItems[] = [
        'notes' => $lineItem->getName(),
        'quantity' => $lineItem->getQuantity(),
        'cost' => $lineItem->getPrice(),
        'tax_name1' => $vat !== NULL ? $vat->getVatLabel() : '',
        'tax_rate1' => $vat !== NULL ? $vat->getRate() : 0,
      ];
    }
    return [
      'client_id' => $clientId,
      'line_items' => $lineItems,
    ];
  }

}
