<?php

namespace Drupal\invoice_ninja\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure invoice_ninja settings for this site.
 */
class Settings extends ConfigFormBase {

  public const CONFIG_NAME = 'invoice_ninja.settings';
  public const INVOICE_NINJA_URL = 'invoice_ninja_url';
  public const INVOICE_NINJA_API_TOKEN = 'invoice_ninja_api_token';
  public const SYNCHRONIZE_USERS = 'synchronize_users';
  public const INVOICE_NINJA_ADMIN_PASSWORD = 'invoice_ninja_admin_password';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'invoice_ninja_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form[self::INVOICE_NINJA_URL] = [
      '#type' => 'textfield',
      '#title' => $this->t('Invoice Ninja URL'),
      '#default_value' => $this->config(self::CONFIG_NAME)->get(self::INVOICE_NINJA_URL),
      '#description' => $this->t('Invoice Ninja URL'),
      '#required' => TRUE,
    ];
    $form[self::INVOICE_NINJA_API_TOKEN] = [
      '#type' => 'password',
      '#title' => $this->t('API Token'),
      '#default_value' => $this->config(self::CONFIG_NAME)->get(self::INVOICE_NINJA_API_TOKEN),
      '#description' => $this->t('API Token'),
      '#required' => TRUE,
    ];
    $form[self::SYNCHRONIZE_USERS] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Synchronize Users'),
      '#default_value' => $this->config(self::CONFIG_NAME)->get(self::SYNCHRONIZE_USERS),
      '#description' => $this->t('Synchronize users to Invoice Ninja. 
      The users must have the permission to be synced.'),
    ];
    $form[self::INVOICE_NINJA_ADMIN_PASSWORD] = [
      '#type' => 'password',
      '#title' => $this->t('Admin User Password'),
      '#default_value' => $this->config(self::CONFIG_NAME)->get(self::INVOICE_NINJA_ADMIN_PASSWORD),
      '#description' => $this->t('For specific requests we need an administrator password as well.'),
      '#states' => [
        'visible' => [
          ':input[name="' . self::SYNCHRONIZE_USERS . '"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="' . self::SYNCHRONIZE_USERS . '"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config(self::CONFIG_NAME)
      ->set(self::INVOICE_NINJA_URL, $form_state->getValue(self::INVOICE_NINJA_URL))
      ->set(self::INVOICE_NINJA_API_TOKEN, $form_state->getValue(self::INVOICE_NINJA_API_TOKEN))
      ->set(self::SYNCHRONIZE_USERS, $form_state->getValue(self::SYNCHRONIZE_USERS))
      ->set(self::INVOICE_NINJA_ADMIN_PASSWORD, $form_state->getValue(self::INVOICE_NINJA_ADMIN_PASSWORD))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
