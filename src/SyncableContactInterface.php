<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

/**
 * Defines the interface for content entities that can be synced as clients.
 */
interface SyncableContactInterface extends SyncableInterface {

  /**
   * Returns the given name of the client's contact.
   *
   * @return string
   *   The given name.
   */
  public function getGivenName(): string;

  /**
   * Returns the family name of the client's contact.
   *
   * @return string
   *   The family name.
   */
  public function getFamilyName(): string;

  /**
   * Returns the email address of the client's contact.
   *
   * @return string|null
   *   The email address, or NULL if none is available.
   */
  public function getEmail(): ?string;

  /**
   * Returns the phone number of the client's contact.
   *
   * @return string|null
   *   The phone number, or NULL if none is available.
   */
  public function getPhone(): ?string;

}
