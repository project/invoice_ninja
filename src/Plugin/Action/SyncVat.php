<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja\Plugin\Action;

use Drupal\invoice_ninja\SyncableInterface;
use Drupal\invoice_ninja\SyncableVatInterface;
use Drupal\invoice_ninja\VatSynchronizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sync a VAT entity.
 *
 * @Action(
 *   id = "invoice_ninja_sync_vat",
 *   label = @Translation("Sync: VAT"),
 *   description = @Translation("Synchronizes a VAT entity with Invoice Ninja."),
 *   type = "entity"
 * )
 */
class SyncVat extends SyncBase {

  /**
   * The invoice synchronizer.
   *
   * @var \Drupal\invoice_ninja\VatSynchronizer
   */
  protected VatSynchronizer $vatSynchronizer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->vatSynchronizer = $container->get('vat.synchronizer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function isCorrectType(SyncableInterface $object): bool {
    return $object instanceof SyncableVatInterface;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(?SyncableVatInterface $entity = NULL): void {
    if ($entity === NULL) {
      return;
    }
    $this->vatSynchronizer->syncVat($entity);
  }

}
