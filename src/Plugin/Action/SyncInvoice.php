<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja\Plugin\Action;

use Drupal\invoice_ninja\InvoiceSynchronizer;
use Drupal\invoice_ninja\SyncableInterface;
use Drupal\invoice_ninja\SyncableInvoiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sync a invoice entity.
 *
 * @Action(
 *   id = "invoice_ninja_sync_invoice",
 *   label = @Translation("Sync: Invoice"),
 *   description = @Translation("Synchronizes an invoice entity with Invoice Ninja."),
 *   type = "entity"
 * )
 */
class SyncInvoice extends SyncBase {

  /**
   * The invoice synchronizer.
   *
   * @var \Drupal\invoice_ninja\InvoiceSynchronizer
   */
  protected InvoiceSynchronizer $invoiceSynchronizer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->invoiceSynchronizer = $container->get('invoice.synchronizer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function isCorrectType(SyncableInterface $object): bool {
    return $object instanceof SyncableInvoiceInterface;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(?SyncableInvoiceInterface $entity = NULL): void {
    if ($entity === NULL) {
      return;
    }
    $this->invoiceSynchronizer->syncInvoice($entity);
  }

}
