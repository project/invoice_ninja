<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja\Plugin\Action;

use Drupal\invoice_ninja\ClientSynchronizer;
use Drupal\invoice_ninja\SyncableClientInterface;
use Drupal\invoice_ninja\SyncableInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sync a client entity.
 *
 * @Action(
 *   id = "invoice_ninja_sync_client",
 *   label = @Translation("Sync: client"),
 *   description = @Translation("Synchronizes a client entity with Invoice Ninja."),
 *   type = "entity"
 * )
 */
class SyncClient extends SyncBase {

  /**
   * The client synchronizer.
   *
   * @var \Drupal\invoice_ninja\ClientSynchronizer
   */
  protected ClientSynchronizer $clientSynchronizer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->clientSynchronizer = $container->get('client.synchronizer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function isCorrectType(SyncableInterface $object): bool {
    return $object instanceof SyncableClientInterface;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(?SyncableClientInterface $entity = NULL): void {
    if ($entity === NULL) {
      return;
    }
    $this->clientSynchronizer->syncClient($entity);
  }

}
