<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\invoice_ninja\SyncableInterface;

/**
 * Base class for sync action plugins.
 */
abstract class SyncBase extends ConfigurableActionBase {

  /**
   * Verifies, that the object is the correct type for the given action.
   *
   * @return bool
   *   TRUE, if the object has the correct type, FALSE otherwise.
   */
  abstract protected function isCorrectType(SyncableInterface $object): bool;

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    if (!($object instanceof SyncableInterface) || $object->isNew() || !$object->shouldBeSynced() || !$this->isCorrectType($object)) {
      $access_result = AccessResult::forbidden();
    }
    else {
      $account = $account ?? $this->currentUser;
      $access_result = $object->access('update', $account, TRUE);
    }
    return $return_as_object ? $access_result : $access_result->isAllowed();
  }

}
