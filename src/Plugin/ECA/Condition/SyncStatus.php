<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja\Plugin\ECA\Condition;

use Drupal\eca\Plugin\ECA\Condition\ConditionBase;
use Drupal\invoice_ninja\ClientSynchronizer;
use Drupal\invoice_ninja\InvoiceSynchronizer;
use Drupal\invoice_ninja\SyncableClientInterface;
use Drupal\invoice_ninja\SyncableInvoiceInterface;
use Drupal\invoice_ninja\SyncableVatInterface;
use Drupal\invoice_ninja\VatSynchronizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ECA condition plugin to get the sync status of an entity.
 *
 * @EcaCondition(
 *   id = "invoice_ninja_sync_status",
 *   label = @Translation("Sync status"),
 *   description = @Translation("Find out if a given entity has already been synced."),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity", label = @Translation("Entity"))
 *   }
 * )
 */
class SyncStatus extends ConditionBase {

  /**
   * The invoice synchronizer.
   *
   * @var \Drupal\invoice_ninja\InvoiceSynchronizer
   */
  protected InvoiceSynchronizer $invoiceSynchronizer;

  /**
   * The client synchronizer.
   *
   * @var \Drupal\invoice_ninja\ClientSynchronizer
   */
  protected ClientSynchronizer $clientSynchronizer;

  /**
   * The VAT Synchronizer.
   *
   * @var \Drupal\invoice_ninja\VatSynchronizer
   */
  protected VatSynchronizer $vatSynchronizer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->invoiceSynchronizer = $container->get('invoice.synchronizer');
    $instance->clientSynchronizer = $container->get('client.synchronizer');
    $instance->vatSynchronizer = $container->get('vat.synchronizer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    $entity = $this->getValueFromContext('entity');
    if ($entity instanceof SyncableInvoiceInterface) {
      return $this->negationCheck($this->invoiceSynchronizer->isSynced($entity));
    }
    if ($entity instanceof SyncableClientInterface) {
      return $this->negationCheck($this->clientSynchronizer->isSynced($entity));
    }
    if ($entity instanceof SyncableVatInterface) {
      return $this->negationCheck($this->vatSynchronizer->isSynced($entity));
    }
    return $this->negationCheck(FALSE);
  }

}
