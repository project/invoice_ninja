<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\invoice_ninja\Form\Settings;
use InvoiceNinja\Sdk\InvoiceNinja;

/**
 * Provides services to synchronize data with Invoice Ninja.
 */
abstract class SynchronizerBase {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The key/value factory service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected KeyValueFactoryInterface $keyValueFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The Invoice Ninja client.
   *
   * @var \InvoiceNinja\Sdk\InvoiceNinja|null
   */
  private ?InvoiceNinja $invoiceNinja = NULL;

  /**
   * Constructs an InvoiceNinjaApi object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The key/value factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, KeyValueFactoryInterface $keyValueFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->config = $configFactory->get(Settings::CONFIG_NAME);
    $this->keyValueFactory = $keyValueFactory;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Stores the given data, id and current timestamp into the given collection.
   *
   * @param string $collection
   *   The collection of the key/value store.
   * @param string $id
   *   The ID to store.
   * @param string $remoteId
   *   The remote ID of Invoice Ninja.
   */
  protected function storeId(string $collection, string $id, string $remoteId): void {
    $data = [
      'remote_id' => $remoteId,
      'last_sync' => time(),
    ];
    $this->keyValueFactory->get($collection)->set($id, $data);
  }

  /**
   * Gets an Invoice Ninja client.
   *
   * @return \InvoiceNinja\Sdk\InvoiceNinja
   *   The Invoice Ninja client.
   */
  protected function getInvoiceNinjaClient(): InvoiceNinja {
    if ($this->invoiceNinja === NULL) {
      $url = $this->config->get(Settings::INVOICE_NINJA_URL);
      $token = $this->config->get(Settings::INVOICE_NINJA_API_TOKEN);
      $this->invoiceNinja = new InvoiceNinja($token);
      $this->invoiceNinja->setUrl($url);
    }
    return $this->invoiceNinja;
  }

}
