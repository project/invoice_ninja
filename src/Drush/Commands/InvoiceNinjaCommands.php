<?php

namespace Drupal\invoice_ninja\Drush\Commands;

use Drupal\invoice_ninja\UserSynchronizer;
use Drush\Attributes\Command;
use Drush\Attributes\Usage;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file for Invoice Ninja.
 */
final class InvoiceNinjaCommands extends DrushCommands {

  /**
   * Constructs an InvoiceNinjaCommands object.
   */
  public function __construct(
    protected readonly UserSynchronizer $userSynchronizer,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): InvoiceNinjaCommands {
    return new InvoiceNinjaCommands(
      $container->get('user.synchronizer'),
    );
  }

  /**
   * Synchronize all users with the permission "allow user to synchronize".
   */
  #[Command(name: 'invoice_ninja:synchronize_users', aliases: ['insu'])]
  #[Usage(name: 'invoice_ninja:synchronize_users', description: 'Synchronize all users with the permission "allow user to synchronize".')]
  public function synchronizeUsers(): void {
    $this->userSynchronizer->synchronizeUsers();
  }

}
