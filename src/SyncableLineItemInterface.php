<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

/**
 * Defines the interface for content entities that can be added to invoices.
 */
interface SyncableLineItemInterface extends SyncableInterface {

  /**
   * Returns the quantity.
   *
   * @return float
   *   The quantity.
   */
  public function getQuantity(): float;

  /**
   * Returns the price for a single item.
   *
   * @return float
   *   The price for a single item.
   */
  public function getPrice(): float;

  /**
   * Returns the VAT that applies to the item.
   *
   * @return \Drupal\invoice_ninja\SyncableVatInterface|null
   *   The VAT that applies, or NULL otherwise.
   */
  public function getVat(): ?SyncableVatInterface;

  /**
   * Returns the name.
   *
   * @return string
   *   The name.
   */
  public function getName(): string;

}
