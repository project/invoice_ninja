<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

use Drupal\invoice_ninja\Form\Settings;
use Drupal\user\UserInterface;

/**
 * Provides services to synchronize data with Invoice Ninja.
 */
final class UserSynchronizer extends SynchronizerBase {

  protected const INVOICE_NINJA_COLLECTION_USER = 'invoice_ninja.sync_user';

  /**
   * Synchronize all users with the permission "allow user to synchronize".
   */
  public function synchronizeUsers(): void {
    if ($this->config->get(Settings::SYNCHRONIZE_USERS) !== 1) {
      return;
    }
    $this->getInvoiceNinjaClient()->addHeader([
      'X-API-PASSWORD' => $this->config->get(Settings::INVOICE_NINJA_ADMIN_PASSWORD),
    ]);
    /** @var \Drupal\user\UserInterface[] $users */
    $users = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['status' => 1]);
    foreach ($users as $user) {
      $permitted = ($user->getAccountName() !== 'admin' &&
        ($user->hasPermission('access invoice_ninja') || $user->hasPermission('administer invoice_ninja')));
      $clientData = $this->keyValueFactory->get(self::INVOICE_NINJA_COLLECTION_USER)->get((string) $user->id());
      if (!$clientData && !$permitted) {
        // Nothing to do, the user doesn't exist remotely and is not permitted.
        continue;
      }
      if (!$clientData) {
        // User is permitted but doesn't exist remotely.
        $remoteUser = $this->getInvoiceNinjaClient()->users->create($this->getUserData($user));
        $this->storeId(self::INVOICE_NINJA_COLLECTION_USER,
          (string) $user->id(), $remoteUser['data']['id']);
      }
      else {
        // Update user and it's permission.
        $lastSync = $clientData['last_sync'];
        $remoteId = $clientData['remote_id'];
        if ($user->get("changed")->value > $lastSync) {
          $this->getInvoiceNinjaClient()->users->update($remoteId, $this->getUserData($user));
          $this->storeId(self::INVOICE_NINJA_COLLECTION_USER,
            (string) $user->id(), $remoteId);
        }
      }
    }
  }

  /**
   * Gets the data of a given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return array
   *   The user data.
   */
  private function getUserData(UserInterface $user): array {
    return [
      'first_name' => $user->getAccountName(),
      'last_name' => $user->getAccountName(),
      'email' => $user->getEmail(),
      'company_user' => [
        'permissions' => 'create_all,view_all,edit_all',
        'isOwner' => TRUE,
        'is_admin' => $user->hasPermission('administer invoice_ninja'),
        'is_locked' => !$user->get('status')->value,
      ],
    ];
  }

}
