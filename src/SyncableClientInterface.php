<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

use Drupal\address\Plugin\Field\FieldType\AddressItem;

/**
 * Defines the interface for content entities that can be synced as clients.
 *
 * @todo All these methods should only be called, if
 *   \Drupal\invoice_ninja\SyncableInterface::shouldBeSynced was called before
 *   and returned TRUE. That's what implementing objects assume.
 *   Can we enforce that or is this just by convention?
 */
interface SyncableClientInterface extends SyncableInterface {

  /**
   * Returns the address of the client.
   *
   * @return \Drupal\address\Plugin\Field\FieldType\AddressItem
   *   The address of the client.
   */
  public function getAddress(): AddressItem;

  /**
   * Returns the phone number of the client.
   *
   * @return string|null
   *   The phone number, or NULL if none is available.
   */
  public function getPhone(): ?string;

  /**
   * Returns all the contacts of the client.
   *
   * In the array there have to be items
   * of <code>SyncableContactInterface</code>.
   *
   * @return array
   *   All the contacts of the client.
   */
  public function getContacts(): array;

  /**
   * Returns the currency for the client.
   *
   * @return string
   *   The currency.
   */
  public function getCurrency(): string;

  /**
   * Returns the language of the client.
   *
   * @return string
   *   The language.
   */
  public function getLanguage(): string;

  /**
   * Returns status whether that client needs to pay VAT.
   *
   * @return bool
   *   TRUE, if the client needs to pay VAT, FALSE otherwise.
   */
  public function isVatFree(): bool;

}
