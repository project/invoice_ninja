<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

/**
 * Defines the interface for content entities that can be synced as VATs.
 */
interface SyncableVatInterface extends SyncableInterface {

  /**
   * Returns the tax name.
   *
   * @return string
   *   The tax name.
   */
  public function getVatLabel(): string;

  /**
   * Returns the tax rate.
   *
   * @return float
   *   The tax rate.
   */
  public function getRate(): float;

}
