<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

/**
 * Provides services to synchronize data with Invoice Ninja.
 */
final class VatSynchronizer extends SynchronizerBase {

  protected const INVOICE_NINJA_COLLECTION_VAT = 'invoice_ninja.sync_vat';

  /**
   * Return the status if the entity has already been synced.
   *
   * @param \Drupal\invoice_ninja\SyncableVatInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE, if the entity has already been synced, FALSE otherwise.
   */
  public function isSynced(SyncableVatInterface $entity): bool {
    return $this->keyValueFactory->get(self::INVOICE_NINJA_COLLECTION_VAT)
      ->get($entity->id()) !== NULL;
  }

  /**
   * Syncs the VAT with Invoice Ninja.
   *
   * @param \Drupal\invoice_ninja\SyncableVatInterface $vat
   *   The VAT.
   */
  public function syncVat(SyncableVatInterface $vat): void {
    $vatData = $this->keyValueFactory->get(self::INVOICE_NINJA_COLLECTION_VAT)
      ->get($vat->id());
    if (!$vatData) {
      $rate = $vat->getRate();
      foreach ($this->getInvoiceNinjaClient()->tax_rates->all()['data'] ?? [] as $item) {
        if ((float) $item['rate'] === $rate) {
          $vatData = [
            'last_sync' => 0,
            'remote_id' => $item['id'],
          ];
          break;
        }
      }
    }
    if (!$vatData) {
      // @todo Search for existing label, since it causes an error
      $remoteVat = $this->getInvoiceNinjaClient()->tax_rates->create(
        $this->getVatData($vat)
      );
      $this->storeId(self::INVOICE_NINJA_COLLECTION_VAT,
        $vat->id(), $remoteVat['data']['id']);
    }
    else {
      $lastSync = $vatData['last_sync'];
      // @phpstan-ignore-next-line
      if ($vat->getChangedTime() > $lastSync) {
        $remoteVatId = $vatData['remote_id'];
        $this->getInvoiceNinjaClient()->tax_rates->update($remoteVatId,
          $this->getVatData($vat)
        );
        $this->storeId(self::INVOICE_NINJA_COLLECTION_VAT,
          $vat->id(), $remoteVatId);
      }
    }
  }

  /**
   * Gets the data for the API call for a VAT.
   *
   * @param \Drupal\invoice_ninja\SyncableVatInterface $vat
   *   The VAT.
   *
   * @return array
   *   The VAT data for the API.
   */
  private function getVatData(SyncableVatInterface $vat): array {
    return [
      'name' => $vat->getVatLabel(),
      'rate' => $vat->getRate(),
    ];
  }

}
