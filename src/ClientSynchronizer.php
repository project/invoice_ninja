<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

/**
 * Provides services to synchronize data with Invoice Ninja.
 */
final class ClientSynchronizer extends SynchronizerBase {

  protected const INVOICE_NINJA_COLLECTION_CLIENT = 'invoice_ninja.sync_client';

  /**
   * The mapping of countries.
   *
   * @var array
   */
  private array $countryMapper = [];

  /**
   * The mapping of currencies.
   *
   * @var array
   */
  private array $currencyMapper = [];

  /**
   * Return the status if the entity has already been synced.
   *
   * @param \Drupal\invoice_ninja\SyncableClientInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE, if the entity has already been synced, FALSE otherwise.
   */
  public function isSynced(SyncableClientInterface $entity): bool {
    return $this->keyValueFactory->get(self::INVOICE_NINJA_COLLECTION_CLIENT)
      ->get($entity->id()) !== NULL;
  }

  /**
   * Syncs the client with Invoice Ninja.
   *
   * If the client already exists, an update gets performed.
   *
   * @param \Drupal\invoice_ninja\SyncableClientInterface $client
   *   The client.
   *
   * @return string
   *   The remote ID from Invoice Ninja.
   */
  public function syncClient(SyncableClientInterface $client): string {
    $clientData = $this->keyValueFactory->get(self::INVOICE_NINJA_COLLECTION_CLIENT)
      ->get($client->id());
    if (!$clientData) {
      $remoteClient = $this->getInvoiceNinjaClient()->clients->create(
        $this->getClientData($client)
      );
      $remoteId = $remoteClient['data']['id'];
      $this->storeId(self::INVOICE_NINJA_COLLECTION_CLIENT,
        $client->id(), $remoteId);
    }
    else {
      $lastSync = $clientData['last_sync'];
      $remoteId = $clientData['remote_id'];
      // @phpstan-ignore-next-line
      if ($client->getChangedTime() > $lastSync) {
        $this->getInvoiceNinjaClient()->clients->update($remoteId,
          $this->getClientData($client));
        $this->storeId(self::INVOICE_NINJA_COLLECTION_CLIENT,
          $client->id(), $remoteId);
      }
    }
    return $remoteId;
  }

  /**
   * Gets the data for the API call for a client.
   *
   * @param \Drupal\invoice_ninja\SyncableClientInterface $client
   *   The client.
   *
   * @return array
   *   The client data for the API.
   */
  private function getClientData(SyncableClientInterface $client): array {
    return [
      'name' => $client->getAddress()->getOrganization(),
      'address1' => $client->getAddress()->getAddressLine1(),
      'address2' => $client->getAddress()->getAddressLine2(),
      'postal_code' => $client->getAddress()->getPostalCode(),
      'city' => $client->getAddress()->getLocality(),
      'phone' => $client->getPhone(),
      'country_id' => $this->getCountryMapping()[$client->getAddress()->getCountryCode()],
      'settings' => [
        'currency_id' => $this->getCurrencyMapping()[$client->getCurrency()],
      ],
      'contacts' => $this->getContactsData($client),
    ];
  }

  /**
   * Gets the data for the API call for the contacts.
   *
   * @param \Drupal\invoice_ninja\SyncableClientInterface $client
   *   The client.
   *
   * @return array
   *   The contacts data for the API.
   */
  private function getContactsData(SyncableClientInterface $client): array {
    $contacts = [];
    /** @var \Drupal\invoice_ninja\SyncableContactInterface $contact */
    foreach ($client->getContacts() as $contact) {
      $contacts[] = [
        'first_name' => $contact->getGivenName(),
        'last_name' => $contact->getFamilyName(),
        'phone' => $contact->getPhone(),
        'email' => $contact->getEmail(),
      ];
    }
    return $contacts;
  }

  /**
   * Gets the mapping of countries.
   *
   * @return array
   *   The mapping of countries.
   */
  protected function getCountryMapping(): array {
    if (empty($this->countryMapper)) {
      foreach ($this->getInvoiceNinjaClient()->statics->countries() as $country) {
        $this->countryMapper[$country['iso_3166_2']] = $country['id'];
      }
    }
    return $this->countryMapper;
  }

  /**
   * Gets the mapping of currencies.
   *
   * @return array
   *   The mapping of currencies.
   */
  protected function getCurrencyMapping(): array {
    if (empty($this->currencyMapper)) {
      foreach ($this->getInvoiceNinjaClient()->statics->currencies() as $currency) {
        $this->currencyMapper[$currency['code']] = $currency['id'];
      }
    }
    return $this->currencyMapper;
  }

}
