<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

/**
 * Defines the interface for content entities that have matching as invoices.
 */
interface SyncableInvoiceInterface extends SyncableInterface {

  /**
   * Gets the client.
   *
   * @return \Drupal\invoice_ninja\SyncableClientInterface
   *   The client.
   */
  public function getClient(): SyncableClientInterface;

  /**
   * Gets the line items.
   *
   * @return \Drupal\invoice_ninja\SyncableLineItemInterface[]
   *   The line items.
   */
  public function getLineItems(): array;

}
