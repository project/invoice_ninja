<?php

declare(strict_types=1);

namespace Drupal\invoice_ninja;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the interface for content entities that can be synced.
 */
interface SyncableInterface extends ContentEntityInterface {

  /**
   * Returns the status whether the entity should be synced with Invoice Ninja.
   *
   * @return bool
   *   TRUE, if this entity should be synced, FALSE otherwise.
   */
  public function shouldBeSynced(): bool;

}
